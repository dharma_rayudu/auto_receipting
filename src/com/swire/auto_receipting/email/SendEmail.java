package com.swire.auto_receipting.email;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.swire.auto_receipting.properties.ReadPropertyFiles;

/**
 * @author Dharma Rayudu
 *
 */
public class SendEmail {
	private static Logger log = LoggerFactory.getLogger(SendEmail.class);

	public static Message getMessage() {
		log.info("Inside getMessage()");
		
		ReadPropertyFiles properties = new ReadPropertyFiles();
		Properties prop = properties.getProperties();
		Properties props = new Properties();

		props.put("mail.smtp.host", prop.getProperty("email.host"));
		props.put("mail.smtp.auth", prop.getProperty("email.auth"));
		props.put("mail.smtp.port", prop.getProperty("email.port"));

		Session session = Session.getDefaultInstance(props, new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(prop.getProperty("email.user"), prop.getProperty("email.password"));
			}
		});

		Message message = null;

		try {
			message = new MimeMessage(session);
			message.setFrom(new InternetAddress(prop.getProperty("email.from")));
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(prop.getProperty("email.to")));
		} catch (MessagingException m) {
			m.printStackTrace();
		}
		return message;
	}

}