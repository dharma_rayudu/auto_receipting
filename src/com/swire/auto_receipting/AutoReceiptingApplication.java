package com.swire.auto_receipting;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.swire.auto_receipting.files.ReadFilesFromFolder;
import com.swire.auto_receipting.files.ReceiptRecordsExtract;
import com.swire.auto_receipting.model.VirtualAccounts;

/**
 * @author Dharma Rayudu
 *
 */
public class AutoReceiptingApplication {

	private static Logger log = LoggerFactory.getLogger(AutoReceiptingApplication.class);
	
	public static void main(String[] args) {
		log.info("From main method");
		
		ReadFilesFromFolder files = new ReadFilesFromFolder();
		List<VirtualAccounts> list = files.getFiles();

		ReceiptRecordsExtract records = new ReceiptRecordsExtract();
		records.receiptRecordExtract(list);

	}

}
