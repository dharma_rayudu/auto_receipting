package com.swire.auto_receipting.model;

import java.util.List;

/**
 * @author Dharma Rayudu
 *
 */
public class VirtualAccounts {
	private String transaction_reference_number;
	private String account_identification;
	private String statement_or_pagenumber;
	private String opening_balance;
	private String statement_line;
	private String information_to_account_owner;
	private String closing_balance;
	private String closing_available_balance;
	private List<String> account_info86;
	private List<String> statement_line61;
	public VirtualAccounts() {

	}

	public VirtualAccounts(String transaction_reference_number, String account_identification,
			String statement_or_pagenumber, String opening_balance, List<String> statement_line61,
			String closing_balance, String closing_available_balance, List<String> account_info86) {
		super();
		this.transaction_reference_number = transaction_reference_number;
		this.account_identification = account_identification;
		this.statement_or_pagenumber = statement_or_pagenumber;
		this.opening_balance = opening_balance;
		this.statement_line61 = statement_line61;
		this.closing_balance = closing_balance;
		this.closing_available_balance = closing_available_balance;
		this.account_info86 = account_info86;
	}
	
	
	public List<String> getAccount_info86() {
		return account_info86;
	}

	public void setAccount_info86(List<String> account_info86) {
		this.account_info86 = account_info86;
	}

	public String getTransaction_reference_number() {
		return transaction_reference_number;
	}

	public void setTransaction_reference_number(String transaction_reference_number) {
		this.transaction_reference_number = transaction_reference_number;
	}

	public String getAccount_identification() {
		return account_identification;
	}

	public void setAccount_identification(String account_identification) {
		this.account_identification = account_identification;
	}

	public String getStatement_or_pagenumber() {
		return statement_or_pagenumber;
	}

	public void setStatement_or_pagenumber(String statement_or_pagenumber) {
		this.statement_or_pagenumber = statement_or_pagenumber;
	}

	public String getOpening_balance() {
		return opening_balance;
	}

	public void setOpening_balance(String opening_balance) {
		this.opening_balance = opening_balance;
	}

	public String getStatement_line() {
		return statement_line;
	}

	public void setStatement_line(String statement_line) {
		this.statement_line = statement_line;
	}

	public String getInformation_to_account_owner() {
		return information_to_account_owner;
	}

	public void setInformation_to_account_owner(String information_to_account_owner) {
		this.information_to_account_owner = information_to_account_owner;
	}

	public String getClosing_balance() {
		return closing_balance;
	}

	public void setClosing_balance(String closing_balance) {
		this.closing_balance = closing_balance;
	}

	public String getClosing_available_balance() {
		return closing_available_balance;
	}

	public void setClosing_available_balance(String closing_available_balance) {
		this.closing_available_balance = closing_available_balance;
	}

	
	public List<String> getStatement_line61() {
		return statement_line61;
	}

	public void setStatement_line61(List<String> statement_line61) {
		this.statement_line61 = statement_line61;
	}

	@Override
	public String toString() {
		return "VirtualAccounts [transaction_reference_number=" + transaction_reference_number
				+ ", account_identification=" + account_identification + ", statement_or_pagenumber="
				+ statement_or_pagenumber + ", opening_balance=" + opening_balance + ", statement_line="
				+ statement_line + ", information_to_account_owner=" + information_to_account_owner
				+ ", closing_balance=" + closing_balance + ", closing_available_balance=" + closing_available_balance
				+ "]";
	}

}
