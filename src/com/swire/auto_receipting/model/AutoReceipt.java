package com.swire.auto_receipting.model;

import java.util.List;

/**
 * @author Dharma Rayudu
 *
 */
public class AutoReceipt {

	private double amountPaid;
	private String currency;
	private String binNumber;
	private List<String> invoiceNumber;
	
	public double getAmountPaid() {
		return amountPaid;
	}
	public void setAmountPaid(double amountPaid) {
		this.amountPaid = amountPaid;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getBinNumber() {
		return binNumber;
	}
	public void setBinNumber(String binNumber) {
		this.binNumber = binNumber;
	}
	public List<String> getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(List<String> invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	@Override
	public String toString() {
		return "AutoReceipt [amountPaid=" + amountPaid + ", currency=" + currency + ", binNumber=" + binNumber
				+ ", invoiceNumber=" + invoiceNumber + "]";
	}
	
	
	
}
