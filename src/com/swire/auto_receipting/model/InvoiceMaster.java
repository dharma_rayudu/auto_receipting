package com.swire.auto_receipting.model;


/**
 * @author Dharma Rayudu
 *
 */
public class InvoiceMaster {

	private String invoiceMaster_no;
	private String vessel;
	private String voyage;
	private String leg;
	private String place_payment;
	private String invoice_currency;
	private String pay_status;
	private String extract_status;
	private String invoice_id;
	private String bl_no;
	private String lastUpdateDate;
	
	public String getInvoiceMaster_no() {
		return invoiceMaster_no;
	}
	public void setInvoiceMaster_no(String invoiceMaster_no) {
		this.invoiceMaster_no = invoiceMaster_no;
	}
	public String getVessel() {
		return vessel;
	}
	public void setVessel(String vessel) {
		this.vessel = vessel;
	}
	public String getVoyage() {
		return voyage;
	}
	public void setVoyage(String voyage) {
		this.voyage = voyage;
	}
	public String getLeg() {
		return leg;
	}
	public void setLeg(String leg) {
		this.leg = leg;
	}
	public String getPlace_payment() {
		return place_payment;
	}
	public void setPlace_payment(String place_payment) {
		this.place_payment = place_payment;
	}
	public String getInvoice_currency() {
		return invoice_currency;
	}
	public void setInvoice_currency(String invoice_currency) {
		this.invoice_currency = invoice_currency;
	}
	public String getPay_status() {
		return pay_status;
	}
	public void setPay_status(String pay_status) {
		this.pay_status = pay_status;
	}
	public String getExtract_status() {
		return extract_status;
	}
	public void setExtract_status(String extract_status) {
		this.extract_status = extract_status;
	}
	public String getInvoice_id() {
		return invoice_id;
	}
	public void setInvoice_id(String invoice_id) {
		this.invoice_id = invoice_id;
	}
	public String getBl_no() {
		return bl_no;
	}
	public void setBl_no(String bl_no) {
		this.bl_no = bl_no;
	}
	public String getLastUpdateDate() {
		return lastUpdateDate;
	}
	public void setLastUpdateDate(String lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	
	
	
}
