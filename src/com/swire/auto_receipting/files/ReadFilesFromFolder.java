package com.swire.auto_receipting.files;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.swire.auto_receipting.database.DatabaseConnection;
import com.swire.auto_receipting.model.VirtualAccounts;
import com.swire.auto_receipting.properties.ReadPropertyFiles;

/**
 * @author Dharma Rayudu
 *
 *         This class reading the MT940 files from folder and extract required
 *         tags from the files.
 */
public class ReadFilesFromFolder {

	private static Logger log = LoggerFactory.getLogger(ReadFilesFromFolder.class);

	ReadPropertyFiles properties = new ReadPropertyFiles();
	Properties props = null;
	private List<String> getAllFilesFromDirectory() {
		log.info("Inside getAllFilesFromDirectory()");
		 props = properties.getProperties();
		 
		List<String> allFiles = null;
		try (Stream<Path> files = Files.walk(Paths.get(props.getProperty("inputFile")))) {

			allFiles = files.filter(Files::isRegularFile).map(file -> file.toString()).collect(Collectors.toList());

		} catch (IOException e) {

			e.printStackTrace();
		}

		return allFiles;
	}

	// This method adding all the required tags to the list.
	public List<VirtualAccounts> getFiles() {
		log.info("Inside getFiles()");
		List<VirtualAccounts> list = new ArrayList<VirtualAccounts>();
		List<String> files = getAllFilesFromDirectory();
			if(files.isEmpty()) {
				System.out.println("There are no files");
			}else {
				
		VirtualAccounts accounts = new VirtualAccounts();
		List<String> account_info86 = new ArrayList<String>();
		List<String> statement_line61 = new ArrayList<String>();

		String information_toAccount_owner = null;
		boolean nextValue = false;

		for (int i = 0; i < files.size(); i++) {

			String file = files.get(i);
			log.info("Name of the File :-" + file.substring(21));
			
			Stream<String> fileData = null;
			try {
				fileData = Files.lines(Paths.get(file));
				
			} catch (IOException e) {
				e.printStackTrace();
			}
			moveFiles(props.getProperty("inputFile")+file.substring(21), props.getProperty("outputFile")+file.substring(21));
			int count = 0;
			Iterator<String> fData = fileData.iterator();
			while (fData.hasNext()) {
				String f = fData.next();
				VirtualAccounts acc = null;

				if (!f.equals("-")) {

					if (f.startsWith(":20:")) {
						String value = f.substring(4);
						accounts.setTransaction_reference_number(value);

					} else if (f.startsWith(":25:")) {
						String value = f.substring(4);
						accounts.setAccount_identification(value);

					} else if (f.startsWith(":28:")) {
						String value = f.substring(4);
						accounts.setStatement_or_pagenumber(value);

					} else if (f.startsWith(":60F:")) {
						String value = f.substring(5);
						accounts.setOpening_balance(value);

					} else if (f.startsWith(":62F:")) {
						String value = f.substring(5);
						accounts.setClosing_balance(value);

						if (count > 0) {

							account_info86.add(information_toAccount_owner);
							accounts.setAccount_info86(account_info86);
							count = 0;
						}

					} else if (f.startsWith(":64:")) {
						String value = f.substring(4);
						accounts.setClosing_available_balance(value);

					} else if (f.startsWith(":61:") && f.contains("CD")) {
						String value = f.substring(4);
						statement_line61.add(value);
						accounts.setStatement_line61(statement_line61);

						if (count > 0) {
							account_info86.add(information_toAccount_owner);
							accounts.setAccount_info86(account_info86);
							count = 0;
						}

					} else if (f.startsWith(":86:") && f.contains("FT INCOMING")) {
						String value = f.substring(4);
						information_toAccount_owner = value;
						nextValue = true;

					} else if (f.startsWith(":86:")) {
						nextValue = false;

					} else if (nextValue && !f.startsWith(":")) {
						information_toAccount_owner = information_toAccount_owner + f;
						accounts.setInformation_to_account_owner(information_toAccount_owner);
						count++;

					}

				} else {
					list.add(acc = new VirtualAccounts(accounts.getTransaction_reference_number(),
							accounts.getAccount_identification(), accounts.getStatement_or_pagenumber(),
							accounts.getOpening_balance(), accounts.getStatement_line61(),
							accounts.getClosing_balance(), accounts.getClosing_available_balance(),
							accounts.getAccount_info86()));

					account_info86 = new ArrayList<String>();
					statement_line61 = new ArrayList<String>();

					accounts = new VirtualAccounts();
					nextValue = false;
				}

			}

		}
			
			}
		return list;
			
	}
	
	/**
	 * @param string
	 * @param string2
	 */
	private void moveFiles(String src, String desc) {
		Path result = null;
		try {
		     result = Files.move(Paths.get(src), Paths.get(desc));
			System.out.println(src+" "+desc);
		}catch(IOException e) {
			log.info(e.getMessage());
		}
		
		if(result !=null) {
			System.out.println("Files moved successfully!");
		}else {
			System.out.println("Files moved failded");
		}
	}


}
