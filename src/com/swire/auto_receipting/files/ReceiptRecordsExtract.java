package com.swire.auto_receipting.files;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Transport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.swire.auto_receipting.database.CreateReceiptHeader;
import com.swire.auto_receipting.database.DatabaseConnection;
import com.swire.auto_receipting.email.SendEmail;
import com.swire.auto_receipting.model.AutoReceipt;
import com.swire.auto_receipting.model.VirtualAccounts;

/**
 * @author Dharma Rayudu
 *
 *
 *         This class extracting required data from the tags.
 */
public class ReceiptRecordsExtract {
	private static Logger log = LoggerFactory.getLogger(ReceiptRecordsExtract.class);
	Connection connection = DatabaseConnection.getConnection();

	public void receiptRecordExtract(List<VirtualAccounts> list) {
		log.info("Inside receiptRecordExtract()");

		List<AutoReceipt> listReceipt = new ArrayList<AutoReceipt>();

		list.forEach(data -> {

			if (data.getAccount_info86() != null) {

				List<String> account_info86 = data.getAccount_info86();
				List<String> account_line61 = data.getStatement_line61();
				String currencyDetails = data.getOpening_balance();

				String[] currency1 = currencyDetails.split("[^a-zA-Z]");
				String currency = null;
				try {
					currency = currency1[6];
				} catch (ArrayIndexOutOfBoundsException a) {
					currency = null;
				}

				Iterator<String> info61 = account_line61.iterator();
				Iterator<String> info86 = account_info86.iterator();

				// This loop is going to check tag61 and 86 has the value
				while (info61.hasNext() && info86.hasNext()) {
					AutoReceipt autoReceipt = new AutoReceipt();
					String accountLine61 = info61.next();
					String accountInfoData86 = info86.next();

					String binDetails = accountLine61.substring(12);
					String binNumberFrom61 = null;

					// Using this condition will get the Bin Number.
					if (binDetails.contains("NTRF")) {
						String[] accountInfo = binDetails.split("NTRF");
						String[] bin = accountInfo[1].split("//");
						binNumberFrom61 = bin[0];
					} else if (binDetails.contains("NMSC")) {
						String[] accountInfo = binDetails.split("NMSC");
						String[] bin = accountInfo[1].split("//");
						binNumberFrom61 = bin[0];
					}

					String currency86 = null;
					String invoiceAmount = null;

					// Using this condition will get the Amount and Currency.
					if (accountInfoData86.contains(binNumberFrom61)) {

						if (accountInfoData86.contains("/OA/")) {
							String[] amountAndCurrency = accountInfoData86.split("\\?");
							for (String amount : amountAndCurrency) {
								if (amount.contains("/OA/")) {
									String amountCurrency = amount;
									String[] amountCurrencySplit = amountCurrency.split("/OA/");
									String mainAmountAndCurr = amountCurrencySplit[1];
									System.out.println(mainAmountAndCurr);
									currency86 = mainAmountAndCurr.substring(0, 3);
									invoiceAmount = mainAmountAndCurr.substring(3);

									log.info("Currency86: " + currency86);
									log.info("Invoice Amount: " + invoiceAmount);
								}

							}

						} else {
							System.out.println("OA Is Missing for the Bin: " + binNumberFrom61);
						}

					} else {
						System.out.println("There is missmatch  Bin Nuber in 86:" + binNumberFrom61);
					}

					// String invoiceNumber;
					/*
					 * if (accountInfoData86.contains("SIS")) { String[] invoiceN =
					 * accountInfoData86.split("SIS"); String[] invoiceNum =
					 * invoiceN[1].split("\\?"); invoiceNumber = "SIS" + invoiceNum[0];
					 * System.out.println(invoiceNumber); } else {
					 * System.out.println("SIS is missing:"); invoiceNumber = null; }
					 */

					// From this conditions will get the Invoice number.
					List<String> multipleInvoices = new ArrayList<String>();
					if (accountInfoData86.contains("SIS")) {
						String[] invo = accountInfoData86.split(" ");
						for (String i : invo) {
							if (i.contains("SIS")) {
								String[] invoiceN = i.split("SIS");
								boolean firstLoop = true;
								for (String in : invoiceN) {
									// System.out.println(in);
									if (!firstLoop) {

										if (in.contains("/") && !in.contains("BN")) {

											String[] multiInvoice = in.split("/");
											for (String invoices : multiInvoice) {
												if (invoices.contains("?")) {
													break;
												} else {
													multipleInvoices.add("SIS" + invoices.substring(0, 7));
												}

											}
										} else {
											multipleInvoices.add("SIS" + in.substring(0, 7));
										}

									} else {
										firstLoop = false;
									}

								}

							}

						}
					} else {
						System.out.println("No SIS");
					}

					autoReceipt.setInvoiceNumber(multipleInvoices);
					autoReceipt.setCurrency(currency86);
					autoReceipt.setBinNumber(binNumberFrom61);

					if (invoiceAmount != null && invoiceAmount.contains(",")) {
						char oldChar = ',';
						char newChar = '.';
						String amountWithDot = invoiceAmount.replace(oldChar, newChar);
						autoReceipt.setAmountPaid(Double.parseDouble(amountWithDot));
					}

					listReceipt.add(autoReceipt);

				}

			}
			log.info("List data" + listReceipt);
		});

		validateReceiptRecrods(listReceipt);

	}

	// This method validates all the required fields are not null
	public void validateReceiptRecrods(List<AutoReceipt> autoRecept) {
		log.info("Inside validateReceiptRecrods()");
		CreateReceiptHeader receiptHeader = new CreateReceiptHeader();

		autoRecept.forEach(data -> {
			if (data.getCurrency() == null || data.getAmountPaid() == 0.0 || data.getBinNumber() == null) {
				System.out.println("What is missing :-" + data);

				String customer = null;
				try (Statement stmt = connection.createStatement()) {

					String queryForGettingCustomer = "SELECT CUSTOMER FROM CUSTOMER_TABLE WHERE VIRTUAL_BANK_ACCT='"
							+ data.getBinNumber() + "'";
					ResultSet rs = stmt.executeQuery(queryForGettingCustomer);
					while (rs.next()) {
						customer = rs.getString("CUSTOMER");
					}
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				Message message = SendEmail.getMessage();
				try {
					message.setSubject("Auto Receipt-" + customer + "-MT940 Received.");
					message.setText("MT940 Received. Partial Auto Receipt action taken \n" + " Missing Data Currency: "
							+ data.getCurrency() + "  Amount: " + data.getAmountPaid() + " Bin Nmber: "
							+ data.getCurrency() + " \n"
							+ " Reason: Unable to create receipt due to missing information");
					Transport.send(message);
					System.out.println("Message sent successfully....");
				} catch (MessagingException e) {
					e.printStackTrace();
				}

			} else if (!data.getInvoiceNumber().isEmpty()) {

				log.info("Create allocated auto receipt");
				receiptHeader.createAutoReceiptAndAllocateInvoice(data);
			} else {

				log.info("Create Unallocated auto receipt");
				receiptHeader.createAutoReceiptAndUnAllocateInvoice(data);
			}
		});

	}

}
