package com.swire.auto_receipting.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.swire.auto_receipting.properties.ReadPropertyFiles;

/**
 * @author Dharma Rayudu
 *
 */
public class DatabaseConnection {

	private static Logger log = LoggerFactory.getLogger(DatabaseConnection.class);

	public static Connection getConnection() {
		log.info("Inside getConnection()");
		ReadPropertyFiles properties = new ReadPropertyFiles();
		Properties prop = properties.getProperties();

		Connection connection = null;

		try {
			Class.forName(prop.getProperty("db.driver"));
			connection = DriverManager.getConnection(prop.getProperty("db.url"), prop.getProperty("db.userName"),
					prop.getProperty("db.password"));

		} catch (Exception e) {
			e.printStackTrace();
		}
		return connection;
	}

}
