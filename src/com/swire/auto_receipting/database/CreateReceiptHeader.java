package com.swire.auto_receipting.database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.mail.Message;
import javax.mail.Transport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.swire.auto_receipting.email.SendEmail;
import com.swire.auto_receipting.model.AutoReceipt;
import com.swire.auto_receipting.model.InvoiceMaster;

/**
 * @author Dharma Rayudu
 *
 *         This class going to create receipt header and allocate the invoice.
 */
public class CreateReceiptHeader {
	private static Logger log = LoggerFactory.getLogger(CreateReceiptHeader.class);

	Connection connection = DatabaseConnection.getConnection();

	LocalDate today = LocalDate.now();
	String todayDate = today.format(DateTimeFormatter.ofPattern("dd-MM-yy"));

	String systemUser = System.getProperty("user.name");

	Integer updateTime = (int) (System.currentTimeMillis() / 1000 / 60);

	public void createAutoReceiptAndAllocateInvoice(AutoReceipt data) {
		log.info("Inside createAutoReceiptAndAllocateInvoice()");

		try (Statement stmt = connection.createStatement()) {
			String binNumber = data.getBinNumber();

			String queryForGettingCustomer = "SELECT CUSTOMER FROM CUSTOMER_TABLE WHERE VIRTUAL_BANK_ACCT='"
					+ data.getBinNumber() + "'";

			ResultSet resultSet = stmt.executeQuery(queryForGettingCustomer);
			String customer = null;
			while (resultSet.next()) {
				customer = resultSet.getString("CUSTOMER");
			}

			String payer = null;
			double invoiceTotalAmount = 0;

			for (String invoiceNumber : data.getInvoiceNumber()) {

				String queryForGettingPayer = "SELECT PAYER, INVOICE_AMOUNT  FROM INVOICE_MASTER WHERE trim(INVOICE_MASTER_NO)=trim('"
						+ invoiceNumber + "')";

				ResultSet resultSetPayer = stmt.executeQuery(queryForGettingPayer);
				while (resultSetPayer.next()) {
					payer = resultSetPayer.getString("PAYER");
					invoiceTotalAmount += resultSetPayer.getDouble("INVOICE_AMOUNT");
				}

				if (!customer.equals(payer)) {
					System.out.println("Customer and Payer are not same");
				}
			}

			if (customer.equals(payer)) {
				log.info("Boath Customers are same");

				if (data.getAmountPaid() == invoiceTotalAmount) {

					String receipt_Id = "SELECT TW.SEQ_RECEIPT_ID.NEXTVAL AS RECEIPT_ID FROM DUAL";

					ResultSet rsReceipt = stmt.executeQuery(receipt_Id);
					long receipt_No = 0;
					while (rsReceipt.next()) {
						receipt_No = rsReceipt.getLong("RECEIPT_ID");
					}

					boolean firstForLoop = true;
					for (String invoiceNumber : data.getInvoiceNumber()) {

						if (!firstForLoop) {

							String qeryForInvoiceM = "SELECT INVOICE_MASTER_ID FROM INVOICE_MASTER WHERE INVOICE_MASTER_NO='"
									+ invoiceNumber.trim() + "'";
							ResultSet rsMaster = stmt.executeQuery(qeryForInvoiceM);

							int master_Id = 0;
							while (rsMaster.next()) {
								master_Id = rsMaster.getInt("INVOICE_MASTER_ID");
							}

							String queryForInvoice = "SELECT IM.INVOICE_MASTER_NO, IM.VESSEL, IM.VOYAGE, IM.LEG, IM.PLACE_PAYMENT, IM.INVOICE_CURRENCY, IM.PAY_STATUS, IM.EXTRACT_STATUS, IM.LAST_UPDATE_DATE, IM.INVOICE_CURRENCY, "
									+ "IH.INVOICE_ID, IH.BL_NO FROM INVOICE_MASTER IM, INVOICE_HEADER IH WHERE IM.INVOICE_MASTER_NO ='"
									+ invoiceNumber.trim() + "' AND IH.INVOICE_MASTER_ID='" + master_Id + "'";

							ResultSet rsInvoice = stmt.executeQuery(queryForInvoice);
							InvoiceMaster invoice = new InvoiceMaster();

							while (rsInvoice.next()) {
								invoice.setInvoiceMaster_no(rsInvoice.getString("INVOICE_MASTER_NO"));
								invoice.setVessel(rsInvoice.getString("VESSEL"));
								invoice.setVoyage(rsInvoice.getString("VOYAGE"));
								invoice.setLeg(rsInvoice.getString("LEG"));
								invoice.setPlace_payment(rsInvoice.getString("PLACE_PAYMENT"));
								invoice.setInvoice_currency(rsInvoice.getString("INVOICE_CURRENCY"));
								invoice.setPay_status(rsInvoice.getString("PAY_STATUS"));
								invoice.setExtract_status(rsInvoice.getString("EXTRACT_STATUS"));
								invoice.setInvoice_id(rsInvoice.getString("INVOICE_ID"));
								invoice.setBl_no(rsInvoice.getString("BL_NO"));
								invoice.setLastUpdateDate(rsInvoice.getString("LAST_UPDATE_DATE"));
								invoice.setInvoice_currency(rsInvoice.getString("INVOICE_CURRENCY"));
							}

							String receiptLine_Id = "SELECT TW.SEQ_RECEIPT_LINE_ID.NEXTVAL AS RECEIPT_LINE_ID FROM DUAL";

							ResultSet rsForLine_Id = stmt.executeQuery(receiptLine_Id);
							long receiptLine_No = 0;
							while (rsForLine_Id.next()) {
								receiptLine_No = rsForLine_Id.getLong("RECEIPT_LINE_ID");
							}

							String queryForReceiptDetail = "INSERT INTO RECEIPT_DETAIL_TABLE VALUES(" + receipt_No
									+ ", " + receiptLine_No + ", '" + invoiceNumber.trim() + "','" + invoice.getVessel()
									+ "', '" + invoice.getVoyage() + "', " + " '" + invoice.getLeg() + "', '"
									+ invoice.getBl_no() + "', to_date('" + todayDate + "', 'dd-MM-YY'), 'null', '"
									+ invoice.getPlace_payment() + "', '" + invoice.getInvoice_currency() + "' , "
									+ " '" + data.getAmountPaid() + "', 'USD', '" + data.getAmountPaid() + "', '"
									+ data.getCurrency() + "', '" + data.getAmountPaid() + "', '" + data.getAmountPaid()
									+ "', '" + data.getAmountPaid() + "', 0, null, 'EXG', " + " '" + systemUser
									+ "', to_date('" + todayDate + "', 'dd-MM-YY'), " + updateTime + ", 'DR', '"
									+ invoice.getPay_status() + "', '" + invoice.getInvoice_id()
									+ "', null, null, null, '" + invoice.getInvoice_id() + "', null, 0, 0, " + " '"
									+ customer + "', 0, null, 0, '" + invoice.getExtract_status()
									+ "', null, null, null, null)";

							String queryForReceiptPaid = "INSERT INTO RECEIPT_PAID VALUES(" + receipt_No + ", "
									+ receiptLine_No + ", " + data.getAmountPaid() + ", '" + systemUser + "', to_date('"
									+ todayDate + "', 'dd-MM-YY'), " + updateTime + ", " + invoice.getInvoice_id()
									+ ", null, null, " + invoice.getInvoice_id() + ", 0)";

							log.info("ReceiptDetail: " + queryForReceiptDetail);
							log.info("ReceiptPaid: " + queryForReceiptPaid);

							try {
								connection.setAutoCommit(false);
								stmt.addBatch(queryForReceiptDetail);
								stmt.addBatch(queryForReceiptPaid);
								stmt.executeBatch();
								connection.commit();

							} catch (SQLException s) {
								log.info(s.getMessage());
								connection.rollback();
							}

						} else {

							String qeryForInvoice_Master = "SELECT INVOICE_MASTER_ID FROM INVOICE_MASTER WHERE INVOICE_MASTER_NO='"
									+ invoiceNumber.trim() + "'";
							ResultSet rsMaster_Data = stmt.executeQuery(qeryForInvoice_Master);

							int master_Id = 0;
							while (rsMaster_Data.next()) {
								master_Id = rsMaster_Data.getInt("INVOICE_MASTER_ID");
							}

							String queryForInvoice_Data = "SELECT IM.INVOICE_MASTER_NO, IM.VESSEL, IM.VOYAGE, IM.LEG, IM.PLACE_PAYMENT, IM.INVOICE_CURRENCY, IM.PAY_STATUS, IM.EXTRACT_STATUS, IM.LAST_UPDATE_DATE, IM.INVOICE_CURRENCY, "
									+ "IH.INVOICE_ID, IH.BL_NO FROM INVOICE_MASTER IM, INVOICE_HEADER IH WHERE IM.INVOICE_MASTER_NO ='"
									+ invoiceNumber.trim() + "' AND IH.INVOICE_MASTER_ID='" + master_Id + "'";

							ResultSet rsInvoice_Data = stmt.executeQuery(queryForInvoice_Data);
							InvoiceMaster invoice_Data = new InvoiceMaster();

							while (rsInvoice_Data.next()) {
								invoice_Data.setInvoiceMaster_no(rsInvoice_Data.getString("INVOICE_MASTER_NO"));
								invoice_Data.setVessel(rsInvoice_Data.getString("VESSEL"));
								invoice_Data.setVoyage(rsInvoice_Data.getString("VOYAGE"));
								invoice_Data.setLeg(rsInvoice_Data.getString("LEG"));
								invoice_Data.setPlace_payment(rsInvoice_Data.getString("PLACE_PAYMENT"));
								invoice_Data.setInvoice_currency(rsInvoice_Data.getString("INVOICE_CURRENCY"));
								invoice_Data.setPay_status(rsInvoice_Data.getString("PAY_STATUS"));
								invoice_Data.setExtract_status(rsInvoice_Data.getString("EXTRACT_STATUS"));
								invoice_Data.setInvoice_id(rsInvoice_Data.getString("INVOICE_ID"));
								invoice_Data.setBl_no(rsInvoice_Data.getString("BL_NO"));
								invoice_Data.setLastUpdateDate(rsInvoice_Data.getString("LAST_UPDATE_DATE"));
								invoice_Data.setInvoice_currency(rsInvoice_Data.getString("INVOICE_CURRENCY"));
							}

							String receiptLineId_Sequence = "SELECT TW.SEQ_RECEIPT_LINE_ID.NEXTVAL AS RECEIPT_LINE_ID FROM DUAL";
							String autoRefNext_No = "SELECT NEXT_NO FROM AUTO_REF_DOC_TABLE WHERE COMPANY='SIS'";

							ResultSet rsForLineId_Sequence = stmt.executeQuery(receiptLineId_Sequence);
							long receiptLineNo_Sequence = 0;
							while (rsForLineId_Sequence.next()) {
								receiptLineNo_Sequence = rsForLineId_Sequence.getLong("RECEIPT_LINE_ID");
							}

							ResultSet rsNext_Number = stmt.executeQuery(autoRefNext_No);
							long autoRefNext_Number = 0;
							while (rsNext_Number.next()) {
								autoRefNext_Number = rsNext_Number.getLong("NEXT_NO");
							}
							String receiptNo_Latest = "SIS" + autoRefNext_Number;
							autoRefNext_Number++;

							String queryForReceiptHeader = "INSERT INTO RECEIPT_HEADER_TABLE VALUES(" + receipt_No
									+ ", '" + receiptNo_Latest + "', 'SIS', 'SIS', 'USDSG', null , '" + customer
									+ "', to_date('" + todayDate + "', 'dd-MM-YY'), '222', 'RCPT', 'TT', 'TEST CODE', "
									+ data.getAmountPaid() + " , '" + data.getCurrency() + "', 1 ,'D', 0, 'USD','A', '"
									+ systemUser + "', to_date('" + todayDate + "', 'dd-MM-YY'), " + updateTime
									+ ", 'DR', " + " 0 , 'JSS', '" + systemUser
									+ "', 'USDSG', 'PAYER', null, 'Test Code', to_date('" + todayDate
									+ "','dd-MM-YY'), 'N', 'Not applicable','N', 0, 0 )";

							String queryForReceiptDetail = "INSERT INTO RECEIPT_DETAIL_TABLE VALUES(" + receipt_No
									+ ", " + receiptLineNo_Sequence + ", '" + invoiceNumber.trim() + "','"
									+ invoice_Data.getVessel() + "', '" + invoice_Data.getVoyage() + "', " + " '"
									+ invoice_Data.getLeg() + "', '" + invoice_Data.getBl_no() + "', to_date('"
									+ todayDate + "', 'dd-MM-YY'), 'null', '" + invoice_Data.getPlace_payment() + "', '"
									+ invoice_Data.getInvoice_currency() + "' , " + " '" + data.getAmountPaid()
									+ "', 'USD', '" + data.getAmountPaid() + "', '" + data.getCurrency() + "', '"
									+ data.getAmountPaid() + "', '" + data.getAmountPaid() + "', '"
									+ data.getAmountPaid() + "', 0, null, 'EXG', " + " '" + systemUser + "', to_date('"
									+ todayDate + "', 'dd-MM-YY'), " + updateTime + ", 'DR', '"
									+ invoice_Data.getPay_status() + "', '" + invoice_Data.getInvoice_id()
									+ "', null, null, null, '" + invoice_Data.getInvoice_id() + "', null, 0, 0, " + " '"
									+ customer + "', 0, null, 0, '" + invoice_Data.getExtract_status()
									+ "', null, null, null, null)";

							String queryForReceiptPaid = "INSERT INTO RECEIPT_PAID VALUES(" + receipt_No + ", "
									+ receiptLineNo_Sequence + ", " + data.getAmountPaid() + ", '" + systemUser
									+ "', to_date('" + todayDate + "', 'dd-MM-YY'), " + updateTime + ", "
									+ invoice_Data.getInvoice_id() + ", null, null, " + invoice_Data.getInvoice_id()
									+ ", 0)";

							String updateAutoRefNext = "UPDATE AUTO_REF_DOC_TABLE SET NEXT_NO=" + autoRefNext_Number
									+ " WHERE COMPANY='SIS'";

							log.info("ReceiptHeader: " + queryForReceiptHeader);
							log.info("ReceiptDetail: " + queryForReceiptDetail);
							log.info("ReceiptPaid: " + queryForReceiptPaid);
							log.info("Update Auto Ref: " + updateAutoRefNext);

							try {
								connection.setAutoCommit(false);
								stmt.addBatch(queryForReceiptHeader);
								stmt.addBatch(queryForReceiptDetail);
								stmt.addBatch(queryForReceiptPaid);
								stmt.addBatch(updateAutoRefNext);
								stmt.executeBatch();
								connection.commit();

							} catch (SQLException s) {
								System.out.println(s);
								connection.rollback();
							}
							firstForLoop = false;
						}

					}
				} else if (data.getAmountPaid() < invoiceTotalAmount) {
					String receiptLineId_Sequence = "SELECT TW.SEQ_RECEIPT_LINE_ID.NEXTVAL AS RECEIPT_LINE_ID FROM DUAL";
					String autoRefNext_No = "SELECT NEXT_NO FROM AUTO_REF_DOC_TABLE WHERE COMPANY='SIS'";

					ResultSet rsForLine_Id = stmt.executeQuery(receiptLineId_Sequence);
					long receiptLine_No = 0;
					while (rsForLine_Id.next()) {
						receiptLine_No = rsForLine_Id.getLong("RECEIPT_LINE_ID");
					}

					ResultSet rsNext_Number = stmt.executeQuery(autoRefNext_No);
					long autoRefNext_Number = 0;
					while (rsNext_Number.next()) {
						autoRefNext_Number = rsNext_Number.getLong("NEXT_NO");
					}
					String receiptNo_Latest = "SIS" + autoRefNext_Number;
					autoRefNext_Number++;

					String queryForReceiptHeader = "INSERT INTO RECEIPT_HEADER_TABLE VALUES(TW.SEQ_RECEIPT_ID.NEXTVAL, '"
							+ receiptNo_Latest + "', 'SIS', 'SIS', 'USDSG', null , '" + customer + "', to_date('"
							+ todayDate + "', 'dd-MM-YY'), '222', 'RCPT', 'TT', 'TEST CODE', " + data.getAmountPaid()
							+ " , '" + data.getCurrency() + "', 1 ,'D', 0, 'USD','A', '" + systemUser + "', to_date('"
							+ todayDate + "', 'dd-MM-YY'), " + updateTime + ", 'DR', " + " 0 , 'JSS', '" + systemUser
							+ "', 'USDSG', 'PAYER', null, 'Test Code', to_date('" + todayDate
							+ "','dd-MM-YY'), 'N', 'Not applicable','N', 0, 0 )";

					String updateAutoRefNext = "UPDATE AUTO_REF_DOC_TABLE SET NEXT_NO=" + autoRefNext_Number
							+ " WHERE COMPANY='SIS'";

					try {
						connection.setAutoCommit(false);
						stmt.addBatch(queryForReceiptHeader);
						stmt.addBatch(updateAutoRefNext);
						stmt.executeBatch();
						connection.commit();

					} catch (SQLException s) {
						System.out.println(s);
						connection.rollback();
					}

					System.out.println("Send notification.");
					System.out.println("Notification will be sent");
					Message message = SendEmail.getMessage();
					message.setSubject(
							"Auto Receipt-[" + customer + "]-MT940 Received. No/ Paartial Auto Receipt action taken.");
					message.setText("MT940 Received. Partial Auto Receipt action taken \n" + " " + customer + " \n"
							+ " " + data.getCurrency() + " " + data.getAmountPaid() + " \n" + " " + receiptLine_No
							+ " \n"
							+ "  Reasion: Unallocated receipt header created. Need to create receipt header with missing amount.");
					Transport.send(message);
					System.out.println("Message sent successfully....");

				} else if (data.getAmountPaid() > invoiceTotalAmount) {

					String receiptId_Sequence = "SELECT TW.SEQ_RECEIPT_ID.NEXTVAL AS RECEIPT_ID FROM DUAL";

					ResultSet rsForId_Sequence = stmt.executeQuery(receiptId_Sequence);
					long receipt_Id = 0;
					while (rsForId_Sequence.next()) {
						receipt_Id = rsForId_Sequence.getLong("RECEIPT_ID");
					}

					boolean firstForLoop = true;
					for (String invoiceNumber : data.getInvoiceNumber()) {

						if (!firstForLoop) {

							String qeryForInvoiceM = "SELECT INVOICE_MASTER_ID FROM INVOICE_MASTER WHERE INVOICE_MASTER_NO='"
									+ invoiceNumber.trim() + "'";
							ResultSet rsMaster = stmt.executeQuery(qeryForInvoiceM);

							int master_Id = 0;
							while (rsMaster.next()) {
								master_Id = rsMaster.getInt("INVOICE_MASTER_ID");
							}

							String queryForInvoice = "SELECT IM.INVOICE_MASTER_NO, IM.VESSEL, IM.VOYAGE, IM.LEG, IM.PLACE_PAYMENT, IM.INVOICE_CURRENCY, IM.PAY_STATUS, IM.EXTRACT_STATUS, IM.LAST_UPDATE_DATE, IM.INVOICE_CURRENCY, "
									+ "IH.INVOICE_ID, IH.BL_NO FROM INVOICE_MASTER IM, INVOICE_HEADER IH WHERE IM.INVOICE_MASTER_NO ='"
									+ invoiceNumber.trim() + "' AND IH.INVOICE_MASTER_ID='" + master_Id + "'";

							ResultSet rsInvoice = stmt.executeQuery(queryForInvoice);
							InvoiceMaster invoice = new InvoiceMaster();

							while (rsInvoice.next()) {
								invoice.setInvoiceMaster_no(rsInvoice.getString("INVOICE_MASTER_NO"));
								invoice.setVessel(rsInvoice.getString("VESSEL"));
								invoice.setVoyage(rsInvoice.getString("VOYAGE"));
								invoice.setLeg(rsInvoice.getString("LEG"));
								invoice.setPlace_payment(rsInvoice.getString("PLACE_PAYMENT"));
								invoice.setInvoice_currency(rsInvoice.getString("INVOICE_CURRENCY"));
								invoice.setPay_status(rsInvoice.getString("PAY_STATUS"));
								invoice.setExtract_status(rsInvoice.getString("EXTRACT_STATUS"));
								invoice.setInvoice_id(rsInvoice.getString("INVOICE_ID"));
								invoice.setBl_no(rsInvoice.getString("BL_NO"));
								invoice.setLastUpdateDate(rsInvoice.getString("LAST_UPDATE_DATE"));
								invoice.setInvoice_currency(rsInvoice.getString("INVOICE_CURRENCY"));
							}

							String receiptLine_Id = "SELECT TW.SEQ_RECEIPT_LINE_ID.NEXTVAL AS RECEIPT_LINE_ID FROM DUAL";

							ResultSet rsForLine_Id = stmt.executeQuery(receiptLine_Id);
							long receiptLine_No = 0;
							while (rsForLine_Id.next()) {
								receiptLine_No = rsForLine_Id.getLong("RECEIPT_LINE_ID");
							}

							String queryForReceiptDetail = "INSERT INTO RECEIPT_DETAIL_TABLE VALUES(" + receipt_Id
									+ ", " + receiptLine_No + ", '" + invoiceNumber.trim() + "','" + invoice.getVessel()
									+ "', '" + invoice.getVoyage() + "', " + " '" + invoice.getLeg() + "', '"
									+ invoice.getBl_no() + "', to_date('" + todayDate + "', 'dd-MM-YY'), 'null', '"
									+ invoice.getPlace_payment() + "', '" + invoice.getInvoice_currency() + "' , "
									+ " '" + data.getAmountPaid() + "', 'USD', '" + data.getAmountPaid() + "', '"
									+ data.getCurrency() + "', '" + data.getAmountPaid() + "', '" + data.getAmountPaid()
									+ "', '" + data.getAmountPaid() + "', 0, null, 'EXG', " + " '" + systemUser
									+ "', to_date('" + todayDate + "', 'dd-MM-YY'), " + updateTime + ", 'DR', '"
									+ invoice.getPay_status() + "', '" + invoice.getInvoice_id()
									+ "', null, null, null, '" + invoice.getInvoice_id() + "', null, 0, 0, " + " '"
									+ customer + "', 0, null, 0, '" + invoice.getExtract_status()
									+ "', null, null, null, null)";

							String queryForReceiptPaid = "INSERT INTO RECEIPT_PAID VALUES(" + receipt_Id + ", "
									+ receiptLine_No + ", " + data.getAmountPaid() + ", '" + systemUser + "', to_date('"
									+ todayDate + "', 'dd-MM-YY'), " + updateTime + ", " + invoice.getInvoice_id()
									+ ", null, null, " + invoice.getInvoice_id() + ", 0)";

							log.info("ReceiptDetail: " + queryForReceiptDetail);
							log.info("ReceiptPaid: " + queryForReceiptPaid);

							try {
								connection.setAutoCommit(false);
								stmt.addBatch(queryForReceiptDetail);
								stmt.addBatch(queryForReceiptPaid);
								stmt.executeBatch();
								connection.commit();

							} catch (SQLException s) {
								log.info(s.getMessage());
								connection.rollback();
							}

						} else {

							String qeryForInvoice_Master = "SELECT INVOICE_MASTER_ID FROM INVOICE_MASTER WHERE INVOICE_MASTER_NO='"
									+ invoiceNumber.trim() + "'";
							ResultSet rsMaster_Data = stmt.executeQuery(qeryForInvoice_Master);

							int master_Id = 0;
							while (rsMaster_Data.next()) {
								master_Id = rsMaster_Data.getInt("INVOICE_MASTER_ID");
							}

							String queryForInvoice_Data = "SELECT IM.INVOICE_MASTER_NO, IM.VESSEL, IM.VOYAGE, IM.LEG, IM.PLACE_PAYMENT, IM.INVOICE_CURRENCY, IM.PAY_STATUS, IM.EXTRACT_STATUS, IM.LAST_UPDATE_DATE, IM.INVOICE_CURRENCY, "
									+ "IH.INVOICE_ID, IH.BL_NO FROM INVOICE_MASTER IM, INVOICE_HEADER IH WHERE IM.INVOICE_MASTER_NO ='"
									+ invoiceNumber.trim() + "' AND IH.INVOICE_MASTER_ID='" + master_Id + "'";

							ResultSet rsInvoice_Data = stmt.executeQuery(queryForInvoice_Data);
							InvoiceMaster invoice_Data = new InvoiceMaster();

							while (rsInvoice_Data.next()) {
								invoice_Data.setInvoiceMaster_no(rsInvoice_Data.getString("INVOICE_MASTER_NO"));
								invoice_Data.setVessel(rsInvoice_Data.getString("VESSEL"));
								invoice_Data.setVoyage(rsInvoice_Data.getString("VOYAGE"));
								invoice_Data.setLeg(rsInvoice_Data.getString("LEG"));
								invoice_Data.setPlace_payment(rsInvoice_Data.getString("PLACE_PAYMENT"));
								invoice_Data.setInvoice_currency(rsInvoice_Data.getString("INVOICE_CURRENCY"));
								invoice_Data.setPay_status(rsInvoice_Data.getString("PAY_STATUS"));
								invoice_Data.setExtract_status(rsInvoice_Data.getString("EXTRACT_STATUS"));
								invoice_Data.setInvoice_id(rsInvoice_Data.getString("INVOICE_ID"));
								invoice_Data.setBl_no(rsInvoice_Data.getString("BL_NO"));
								invoice_Data.setLastUpdateDate(rsInvoice_Data.getString("LAST_UPDATE_DATE"));
								invoice_Data.setInvoice_currency(rsInvoice_Data.getString("INVOICE_CURRENCY"));
							}

							String receiptLineId_Sequence = "SELECT TW.SEQ_RECEIPT_LINE_ID.NEXTVAL AS RECEIPT_LINE_ID FROM DUAL";
							String autoRefNext_No = "SELECT NEXT_NO FROM AUTO_REF_DOC_TABLE WHERE COMPANY='SIS'";

							ResultSet rsForLineId_Sequence = stmt.executeQuery(receiptLineId_Sequence);
							long receiptLineNo_Sequence = 0;
							while (rsForLineId_Sequence.next()) {
								receiptLineNo_Sequence = rsForLineId_Sequence.getLong("RECEIPT_LINE_ID");
							}

							ResultSet rsNext_Number = stmt.executeQuery(autoRefNext_No);
							long autoRefNext_Number = 0;
							while (rsNext_Number.next()) {
								autoRefNext_Number = rsNext_Number.getLong("NEXT_NO");
							}
							String receiptNo_Latest = "SIS" + autoRefNext_Number;
							autoRefNext_Number++;

							String queryForReceiptHeader = "INSERT INTO RECEIPT_HEADER_TABLE VALUES(" + receipt_Id
									+ ", '" + receiptNo_Latest + "', 'SIS', 'SIS', 'USDSG', null , '" + customer
									+ "', to_date('" + todayDate + "', 'dd-MM-YY'), '222', 'RCPT', 'TT', 'TEST CODE', "
									+ data.getAmountPaid() + " , '" + data.getCurrency() + "', 1 ,'D', 0, 'USD','A', '"
									+ systemUser + "', to_date('" + todayDate + "', 'dd-MM-YY'), " + updateTime
									+ ", 'DR', " + " 0 , 'JSS', '" + systemUser
									+ "', 'USDSG', 'PAYER', null, 'Test Code', to_date('" + todayDate
									+ "','dd-MM-YY'), 'N', 'Not applicable','N', 0, 0 )";

							String queryForReceiptDetail = "INSERT INTO RECEIPT_DETAIL_TABLE VALUES(" + receipt_Id
									+ ", " + receiptLineNo_Sequence + ", '" + invoiceNumber.trim() + "','"
									+ invoice_Data.getVessel() + "', '" + invoice_Data.getVoyage() + "', " + " '"
									+ invoice_Data.getLeg() + "', '" + invoice_Data.getBl_no() + "', to_date('"
									+ todayDate + "', 'dd-MM-YY'), 'null', '" + invoice_Data.getPlace_payment() + "', '"
									+ invoice_Data.getInvoice_currency() + "' , " + " '" + data.getAmountPaid()
									+ "', 'USD', '" + data.getAmountPaid() + "', '" + data.getCurrency() + "', '"
									+ data.getAmountPaid() + "', '" + data.getAmountPaid() + "', '"
									+ data.getAmountPaid() + "', 0, null, 'EXG', " + " '" + systemUser + "', to_date('"
									+ todayDate + "', 'dd-MM-YY'), " + updateTime + ", 'DR', '"
									+ invoice_Data.getPay_status() + "', '" + invoice_Data.getInvoice_id()
									+ "', null, null, null, '" + invoice_Data.getInvoice_id() + "', null, 0, 0, " + " '"
									+ customer + "', 0, null, 0, '" + invoice_Data.getExtract_status()
									+ "', null, null, null, null)";

							String queryForReceiptPaid = "INSERT INTO RECEIPT_PAID VALUES(" + receipt_Id + ", "
									+ receiptLineNo_Sequence + ", " + data.getAmountPaid() + ", '" + systemUser
									+ "', to_date('" + todayDate + "', 'dd-MM-YY'), " + updateTime + ", "
									+ invoice_Data.getInvoice_id() + ", null, null, " + invoice_Data.getInvoice_id()
									+ ", 0)";

							String updateAutoRefNext = "UPDATE AUTO_REF_DOC_TABLE SET NEXT_NO=" + autoRefNext_Number
									+ " WHERE COMPANY='SIS'";

							log.info("ReceiptHeader: " + queryForReceiptHeader);
							log.info("ReceiptDetail: " + queryForReceiptDetail);
							log.info("ReceiptPaid: " + queryForReceiptPaid);
							log.info("Update Auto Ref: " + updateAutoRefNext);

							try {
								connection.setAutoCommit(false);
								stmt.addBatch(queryForReceiptHeader);
								stmt.addBatch(queryForReceiptDetail);
								stmt.addBatch(queryForReceiptPaid);
								stmt.addBatch(updateAutoRefNext);
								stmt.executeBatch();
								connection.commit();

							} catch (SQLException s) {
								log.info(s.getMessage());
								connection.rollback();
							}
							firstForLoop = false;
						}

					}

					System.out.println("Send notification for unallocated amount.");
					Message message = SendEmail.getMessage();
					message.setSubject("Auto Receipt-[" + customer + "]-MT940 Received.");
					message.setText("MT940 Received. Partial Auto Receipt action taken \n" + " " + customer + " \n"
							+ " " + data.getCurrency() + " " + data.getAmountPaid() + " \n"
							+ "  Reason: Allocated receipt header created. Need to create receipt header with missing amount.");
					Transport.send(message);
					System.out.println("Message sent successfully....");

				}

			}

		} catch (Exception e) {
			System.out.println(e.toString());
		}
	}

	public void createAutoReceiptAndUnAllocateInvoice(AutoReceipt data) {
		log.info("Inside createAutoReceiptAndUnAllocateInvoice()");

		try (Statement stmt = connection.createStatement()) {
			log.info("What is the bin nuber: " + data.getBinNumber());
			String binNumber = data.getBinNumber();

			String queryForGettingCustomer = "SELECT CUSTOMER, CUSTOMER_NAME FROM CUSTOMER_TABLE WHERE VIRTUAL_BANK_ACCT='"
					+ binNumber + "'";
			ResultSet resultSet = stmt.executeQuery(queryForGettingCustomer);
			String customer = null;
			String customerName = null;
			while (resultSet.next()) {
				customer = resultSet.getString("CUSTOMER");
				customerName = resultSet.getString("CUSTOMER_NAME");
			}
			log.info("Customer Name: " + customer);

			String autoRefNextNo = "SELECT NEXT_NO FROM AUTO_REF_DOC_TABLE WHERE COMPANY='SIS'";
			ResultSet rsNextNumber = stmt.executeQuery(autoRefNextNo);
			long autoRefNextNumber = 0;
			while (rsNextNumber.next()) {
				autoRefNextNumber = rsNextNumber.getLong("NEXT_NO");
			}
			String receiptNoLatest = "SIS" + autoRefNextNumber;
			autoRefNextNumber++;

			String receiptIdSequence = "SELECT TW.SEQ_RECEIPT_ID.NEXTVAL AS RECEIPT_ID FROM DUAL";
			String receiptLineIdSequence = "SELECT TW.SEQ_RECEIPT_LINE_ID.NEXTVAL AS RECEIPT_LINE_ID FROM DUAL";

			ResultSet rsForIdSequence = stmt.executeQuery(receiptIdSequence);
			long receiptNoSequence = 0;
			while (rsForIdSequence.next()) {
				receiptNoSequence = rsForIdSequence.getLong("RECEIPT_ID");
			}

			ResultSet rsForLineIdSequence = stmt.executeQuery(receiptLineIdSequence);
			long receiptLineNoSequence = 0;
			while (rsForLineIdSequence.next()) {
				receiptLineNoSequence = rsForLineIdSequence.getLong("RECEIPT_LINE_ID");
			}

			String queryForInvoiceCount = "SELECT COUNT(INVOICE_AMOUNT) AS POP FROM INVOICE_MASTER WHERE PAYER='"
					+ customer + "' AND INVOICE_AMOUNT='" + data.getAmountPaid() + "'";
			System.out.println(queryForInvoiceCount);
			ResultSet popCount = stmt.executeQuery(queryForInvoiceCount);
			int count = 0;

			while (popCount.next()) {
				count = popCount.getInt("POP");
			}

			String invoiceNumber = null;
			if (count == 1) {
				String queryForInvoiceNumber = "SELECT INVOICE_MASTER_NO FROM INVOICE_MASTER WHERE PAYER='" + customer
						+ "' AND INVOICE_AMOUNT='" + data.getAmountPaid() + "'";
				System.out.println(queryForInvoiceNumber);
				ResultSet invoice = stmt.executeQuery(queryForInvoiceNumber);

				while (invoice.next()) {
					invoiceNumber = invoice.getString("INVOICE_MASTER_NO");
				}

			}

			String queryForInvoiceData = "SELECT IM.INVOICE_MASTER_NO, IM.VESSEL, IM.VOYAGE, IM.LEG, IM.PLACE_PAYMENT, IM.INVOICE_CURRENCY, IM.PAY_STATUS, IM.EXTRACT_STATUS, IM.LAST_UPDATE_DATE, IM.INVOICE_CURRENCY, "
					+ "IH.INVOICE_ID, IH.BL_NO FROM INVOICE_MASTER IM, INVOICE_HEADER IH WHERE IM.PAYER='" + customer
					+ "' AND IM.INVOICE_AMOUNT='" + data.getAmountPaid()
					+ "' AND IM.INVOICE_MASTER_ID = IH.INVOICE_MASTER_ID ";
			System.out.println("Test query: " + queryForInvoiceData);
			ResultSet rsInvoiceData = stmt.executeQuery(queryForInvoiceData);
			InvoiceMaster invoiceData = new InvoiceMaster();

			while (rsInvoiceData.next()) {
				invoiceData.setInvoiceMaster_no(rsInvoiceData.getString("INVOICE_MASTER_NO"));
				invoiceData.setVessel(rsInvoiceData.getString("VESSEL"));
				invoiceData.setVoyage(rsInvoiceData.getString("VOYAGE"));
				invoiceData.setLeg(rsInvoiceData.getString("LEG"));
				invoiceData.setPlace_payment(rsInvoiceData.getString("PLACE_PAYMENT"));
				invoiceData.setInvoice_currency(rsInvoiceData.getString("INVOICE_CURRENCY"));
				invoiceData.setPay_status(rsInvoiceData.getString("PAY_STATUS"));
				invoiceData.setExtract_status(rsInvoiceData.getString("EXTRACT_STATUS"));
				invoiceData.setInvoice_id(rsInvoiceData.getString("INVOICE_ID"));
				invoiceData.setBl_no(rsInvoiceData.getString("BL_NO"));
				invoiceData.setLastUpdateDate(rsInvoiceData.getString("LAST_UPDATE_DATE"));
				invoiceData.setInvoice_currency(rsInvoiceData.getString("INVOICE_CURRENCY"));
			}

			if (count == 1) {
				System.out.println("Single POP");
				String queryForReceiptHeader = "INSERT INTO RECEIPT_HEADER_TABLE VALUES(" + receiptNoSequence + ", '"
						+ receiptNoLatest + "', 'SIS', 'SIS', 'USDSG', null , '" + customer + "', to_date('" + todayDate
						+ "', 'dd-MM-YY'), '222', 'RCPT', 'TT', 'TEST CODE', " + data.getAmountPaid() + " , '"
						+ data.getCurrency() + "', 1 ,'D', 0, 'USD','A', '" + systemUser + "', to_date('" + todayDate
						+ "', 'dd-MM-YY'), " + updateTime + ", 'DR', " + " 0 , 'JSS', '" + systemUser
						+ "', 'USDSG', 'PAYER', null, 'Test Code', to_date('" + todayDate
						+ "','dd-MM-YY'), 'N', 'Not applicable','N', 0, 0 )";

				String queryForReceiptDetail = "INSERT INTO RECEIPT_DETAIL_TABLE VALUES(" + receiptNoSequence + ", "
						+ receiptLineNoSequence + ", '" + invoiceNumber + "','" + invoiceData.getVessel() + "', '"
						+ invoiceData.getVoyage() + "', " + " '" + invoiceData.getLeg() + "', '"
						+ invoiceData.getBl_no() + "', to_date('" + todayDate + "', 'dd-MM-YY'), 'null', '"
						+ invoiceData.getPlace_payment() + "', '" + invoiceData.getInvoice_currency() + "' , " + " '"
						+ data.getAmountPaid() + "', 'USD', '" + data.getAmountPaid() + "', '" + data.getCurrency()
						+ "', '" + data.getAmountPaid() + "', '" + data.getAmountPaid() + "', '" + data.getAmountPaid()
						+ "', 0, null, 'EXG', " + " '" + systemUser + "', to_date('" + todayDate + "', 'dd-MM-YY'), "
						+ updateTime + ", 'DR', '" + invoiceData.getPay_status() + "', '" + invoiceData.getInvoice_id()
						+ "', null, null, null, '" + invoiceData.getInvoice_id() + "', null, 0, 0, " + " '" + customer
						+ "', 0, null, 0, '" + invoiceData.getExtract_status() + "', null, null, null, null)";

				String queryForReceiptPaid = "INSERT INTO RECEIPT_PAID VALUES(" + receiptNoSequence + ", "
						+ receiptLineNoSequence + ", " + data.getAmountPaid() + ", '" + systemUser + "', to_date('"
						+ todayDate + "', 'dd-MM-YY'), " + updateTime + ", " + invoiceData.getInvoice_id()
						+ ", null, null, " + invoiceData.getInvoice_id() + ", 0)";

				String updateAutoRefNext = "UPDATE AUTO_REF_DOC_TABLE SET NEXT_NO=" + autoRefNextNumber
						+ " WHERE COMPANY='SIS'";

				log.info("ReceiptHeader: " + queryForReceiptHeader);
				log.info("ReceiptDetail: " + queryForReceiptDetail);
				log.info("ReceiptPaid: " + queryForReceiptPaid);
				log.info("Update Auto Ref: " + updateAutoRefNext);

				try {
					connection.setAutoCommit(false);
					stmt.addBatch(queryForReceiptHeader);
					stmt.addBatch(queryForReceiptDetail);
					stmt.addBatch(queryForReceiptPaid);
					stmt.addBatch(updateAutoRefNext);
					stmt.executeBatch();
					connection.commit();

				} catch (SQLException s) {
					log.info(s.getMessage());
					connection.rollback();
				}

			} else if (count > 1) {
				log.info("Multip POP");
				String queryForInsertingData = "INSERT INTO RECEIPT_HEADER_TABLE VALUES(TW.SEQ_RECEIPT_ID.NEXTVAL, '"
						+ receiptNoLatest + "', 'SIS', 'SIS', 'USDSG', null , '" + customer + "', to_date('" + todayDate
						+ "', 'dd-MM-YY'), '222', 'RCPT', 'TT', 'TEST CODE', " + data.getAmountPaid() + " , '"
						+ data.getCurrency() + "', 1 ,'D', 0, 'USD','U', '" + systemUser + "', to_date('" + todayDate
						+ "', 'dd-MM-YY'), " + updateTime + ", 'DR', " + " " + data.getAmountPaid() + " , 'JSS', '"
						+ systemUser + "', 'USDSG', 'PAYER', null, 'Test Code', to_date('" + todayDate
						+ "','dd-MM-YY'), 'N', 'Not applicable','N', 0, 0 )";

				String updateAutoRefNext = "UPDATE AUTO_REF_DOC_TABLE SET NEXT_NO=" + autoRefNextNumber
						+ " WHERE COMPANY='SIS'";

				try {
					connection.setAutoCommit(false);
					stmt.addBatch(queryForInsertingData);
					stmt.addBatch(updateAutoRefNext);
					stmt.executeBatch();
					connection.commit();

				} catch (SQLException e) {
					log.info(e.getMessage());
					connection.rollback();
				}
				System.out.println("Notification will be sent");
				Message message = SendEmail.getMessage();
				message.setSubject("Auto Receipt-" + customer + "-MT940 Received.");
				message.setText("MT940 Received. Partial Auto Receipt action taken \n" + " " + data.getBinNumber()
						+ ", " + customer + " - " + customerName + " \n" + " " + data.getCurrency() + " "
						+ data.getAmountPaid() + " \n" + " " + receiptNoLatest + " \n"
						+ "  Reason: Invoice number is missing. Unallocated receipt header is created.");
				Transport.send(message);
				System.out.println("Message sent successfully....");

			} else {

				String queryForInsertingData = "INSERT INTO RECEIPT_HEADER_TABLE VALUES(TW.SEQ_RECEIPT_ID.NEXTVAL, '"
						+ receiptNoLatest + "', 'SIS', 'SIS', 'USDSG', null , '" + customer + "', to_date('" + todayDate
						+ "', 'dd-MM-YY'), '222', 'RCPT', 'TT', 'TEST CODE', " + data.getAmountPaid() + " , '"
						+ data.getCurrency() + "', 1 ,'D', 0, 'USD','U', '" + systemUser + "', to_date('" + todayDate
						+ "', 'dd-MM-YY'), " + updateTime + ", 'DR', " + " " + data.getAmountPaid() + " , 'JSS', '"
						+ systemUser + "', 'USDSG', 'PAYER', null, 'Test Code', to_date('" + todayDate
						+ "','dd-MM-YY'), 'N', 'Not applicable','N', 0, 0 )";

				String updateAutoRefNext = "UPDATE AUTO_REF_DOC_TABLE SET NEXT_NO=" + autoRefNextNumber
						+ " WHERE COMPANY='SIS'";
				log.info("UnAllocate: " + queryForInsertingData);
				log.info("Auto Ref Nex no: " + updateAutoRefNext);
				try {
					connection.setAutoCommit(false);
					stmt.addBatch(queryForInsertingData);
					stmt.addBatch(updateAutoRefNext);
					stmt.executeBatch();
					connection.commit();

				} catch (SQLException e) {
					System.out.println(e);
					connection.rollback();
				}
				System.out.println("Notification will be sent");
				Message message = SendEmail.getMessage();
				message.setSubject("Auto Receipt-" + customer + "-MT940 Received.");
				message.setText("MT940 Received. Partial Auto Receipt action taken \n" + " " + data.getBinNumber()
						+ ", " + customer + " - " + customerName + " \n" + " " + data.getCurrency() + " "
						+ data.getAmountPaid() + " \n" + " " + receiptNoLatest + " \n"
						+ "  Reason: Invoice number is missing. Unallocated receipt header is created.");
				Transport.send(message);
				System.out.println("Message sent successfully....");
			}

		} catch (Exception e) {
			log.info(e.toString());
		}
	}
}
