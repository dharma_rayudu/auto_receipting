/**
 * 
 */
package com.swire.auto_receipting.properties;

import java.io.InputStream;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Dharma Rayudu
 *
 *         This will be reading the properties.
 */
public class ReadPropertyFiles {
	private static Logger log = LoggerFactory.getLogger(ReadPropertyFiles.class);

	static String fileName = "config.properties";

	public Properties getProperties() {
		log.info("Inside getProperties()");
		Properties prop = new Properties();

		try (InputStream inStream = getClass().getClassLoader().getResourceAsStream(fileName)) {

			prop.load(inStream);

		} catch (Exception e) {
			System.out.println(e);
		}

		log.info("Properties" + prop);
		return prop;
	}

}
